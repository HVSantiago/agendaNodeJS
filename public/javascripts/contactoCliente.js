var ContactoCliente = function() {
  var main = this;
  var contactoUri = "http://localhost:3000/api/contacto";
  var categoriaUri = "http://localhost:3000/api/categoria";
  main.contactos = ko.observableArray([]);
  main.categorias = ko.observableArray([]);
  main.error = ko.observable();
  main.contactoCargado = ko.observable();

  main.contactoNuevo = {
    Nombre: ko.observable(),
    Apellido: ko.observable(),
    Direccion: ko.observable(),
    Telefono: ko.observable(),
    Correo: ko.observable(),
    Categoria: ko.observable(),
    IdUsuario: ko.observable()
  }

  main.getAllContactos = function() {
    ajaxHelper(contactoUri, 'GET').done(function(data) {
      main.contactos(data);
    });
  }

  main.getAllCategorias = function () {
    ajaxHelper(categoriaUri, 'GET').done(function (data) {
      main.categorias(data);
    });
  }

  main.agregar = function () {
    var contacto = {
      Nombre: main.contactoNuevo.Nombre(),
      Apellido: main.contactoNuevo.Apellido(),
      Direccion: main.contactoNuevo.Direccion(),
      Telefono: main.contactoNuevo.Telefono(),
      Correo: main.contactoNuevo.Correo(),
      IdCategoria: main.contactoNuevo.Categoria().IdCategoria,
      Categoria: main.contactoNuevo.Categoria(),
      IdUsuario: main.contactoNuevo.IdUsuario()
    }
    ajaxHelper(contactoUri, 'POST', contactos)
    .done(function (data) {
      main.getAllContactos();
      $("#modalAgregarContacto").modal('hide');
    });
  }

  main.editar =function () {
    var contacto = {
      IdContacto: main.contactoCargado().IdContacto,
      Nombre: main.contactoCargado().Nombre,
      Apellido: main.contactoCargado().Apellido,
      Direccion: main.contactoCargado().Direccion,
      Telefono: main.contactoCargado().Telefono,
      Correo: main.contactoCargado().Correo,
      IdCategoria: main.contactoCargado().IdCategoria,
      Categoria: main.contactoCargado().Categoria
    }
    var uriEditar = contactoUri + contacto.IdContacto;
    ajaxHelper(uriEditar, 'PUT', contacto)
    .done(function (data) {
      main.getAllContactos();
    });
  }


  main.eliminar = function (item) {
    var id = item.IdContacto;
    var uri = contactoUri + id;
    ajaxHelper(uri, 'DELETE').done(function () {
      main.getAllContactos()
    });
  }

  main.cargar = function (item) {
    main.contactoCargado(item);
  }

  function ajaxHelper(uri, method, data) {
    return $.ajax({
      url : uri,
      type: method,
      dataType: 'json',
      contentType: 'application/json',
      data: data ? JSON.stringify(data) : null
    }).fail(function(jqXHR, textStatus, errorThrown){
      console.log(errorThrown);
    })
  }



  main.getAllContactos();
  main.getAllCategorias();
}

$(document).ready(function() {
  var contacto = new ContactoCliente();
  ko.applyBindings(contacto);
});
